package ru.tsk.ilina.tm.component;

import ru.tsk.ilina.tm.api.ICommandController;
import ru.tsk.ilina.tm.api.ICommandService;
import ru.tsk.ilina.tm.constant.ArgumentConst;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.controller.CommandController;
import ru.tsk.ilina.tm.repository.CommandRepository;
import ru.tsk.ilina.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    private final ICommandService COMMAND_SERVICE = new CommandService(COMMAND_REPOSITORY);

    private final ICommandController COMMAND_CONTROLLER = new CommandController(COMMAND_SERVICE);

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public void parseArg(String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                COMMAND_CONTROLLER.showAbout();
                break;
            case ArgumentConst.VERSION:
                COMMAND_CONTROLLER.showVersion();
                break;
            case ArgumentConst.HELP:
                COMMAND_CONTROLLER.showHelp();
                break;
            case ArgumentConst.INFO:
                COMMAND_CONTROLLER.showInfo();
                break;
            default:
                COMMAND_CONTROLLER.showErrorCommand();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                COMMAND_CONTROLLER.showAbout();
                break;
            case TerminalConst.VERSION:
                COMMAND_CONTROLLER.showVersion();
                break;
            case TerminalConst.HELP:
                COMMAND_CONTROLLER.showHelp();
                break;
            case TerminalConst.EXIT:
                COMMAND_CONTROLLER.exit();
                break;
            case TerminalConst.INFO:
                COMMAND_CONTROLLER.showInfo();
                break;
        }
    }

    public void start(String[] args) {
        System.out.println("**WELCOME TO TASK MANAGER**");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND: ");
            String command = scanner.nextLine();
            parseCommand(command);
        }
    }

}
