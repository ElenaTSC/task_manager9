package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.ICommandRepository;
import ru.tsk.ilina.tm.api.ICommandService;
import ru.tsk.ilina.tm.model.Command;

public class CommandService implements ICommandService {

    public final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
