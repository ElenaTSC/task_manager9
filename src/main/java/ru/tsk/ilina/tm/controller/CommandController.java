package ru.tsk.ilina.tm.controller;

import ru.tsk.ilina.tm.api.ICommandController;
import ru.tsk.ilina.tm.api.ICommandService;
import ru.tsk.ilina.tm.model.Command;
import ru.tsk.ilina.tm.util.NumberUtil;

public class CommandController implements ICommandController {
    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showErrorCommand() {
        System.out.println("Error! Command not found");
    }

    @Override
    public void showErrorArgument() {
        System.out.println("Error! Argument not supported");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Elena Ilina");
        System.out.println("E-MAIL: eilina@tsconsulting.com");
    }

    @Override
    public void showVersion() {
        System.out.println("1.0.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final Runtime runtime = Runtime.getRuntime();
        System.out.println("Available processors (cores): " + runtime.availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(runtime.freeMemory()));
        long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + NumberUtil.formatBytes(runtime.totalMemory()));
        final long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Use memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public void exit() {
        System.exit(0);
    }
}
